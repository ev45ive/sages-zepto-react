## GIT
git clone https://bitbucket.org/ev45ive/sages-zepto-react.git sages-zepto-react
cd sages-zepto-react
<!-- code sages-zepto-react -->
npm install
npm run start

<!-- updates -->
git stash -u 
git pull 
<!-- git pull -t origin master -->


## NPM semver + package.lock
https://semver.npmjs.com/
https://semver.org/

npm install // update
npm ci // exact package lock

## Instalacje
node -v 
16.13.1

npm -v 
6.14.6

git --version 
git version 2.31.1.windows.1

// Visual Studio Code -> Help -> About
code -v 
1.65.2

Google Chrome	99.0.4844.51

## Create React app

https://create-react-app.dev/

npx create-react-app --help

//  a custom template published on npm: cra-template-typescript:

npx create-react-app "." --template typescript

// alternatives to ejecting 'react-scripts'
https://github.com/gsoft-inc/craco

## UI Toolkits
https://mui.com/
https://react-bootstrap.github.io/components/alerts
https://www.primefaces.org/primereact/
https://ant.design/docs/react/introduce


## Plugins

vs code snippets:
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

chrome devtools:
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi

generate types:
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## Playlists module

mkdir -p src/core/model
touch src/playlists/model/Playlist.ts

mkdir -p src/playlists/containers
touch src/playlists/containers/PlaylistsView.tsx

mkdir -p src/playlists/components
touch src/playlists/components/PlaylistList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistEditor.tsx

## Search module

touch src/core/model/Search.ts
mkdir -p src/search/containers
touch src/search/containers/MusicSearchView.tsx

mkdir -p src/search/components
touch src/search/components/SearchForm.tsx
touch src/search/components/ResultsGrid.tsx
touch src/search/components/AlbumCard.tsx

## Axios
npm i axios

## oAuth2 hook
npm i react-oauth2-hook react-storage-hook immutable

## Request hooks
https://react-query.tanstack.com/quick-start
https://swr.vercel.app/ <!-- next.js -->
https://use-http.com/#/

## React-query
npm i react-query

## React Router
npm install react-router-dom

## Formik
https://formik.org/docs/tutorial
https://github.com/jquense/yup - validator

## Jest
https://jestjs.io/
https://github.com/jsdom/jsdom

## React testing library
https://testing-library.com/docs/react-testing-library/intro/

# Add Storybook:
npx sb init

## Build
npm run build

Pretty url:
https://angular.io/guide/deployment#server-configuration