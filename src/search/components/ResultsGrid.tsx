import React from "react";
import { Album } from "../../core/model/Search";
import AlbumCard from "./AlbumCard";

type Props = { results: Album[]; };

const ResultsGrid = ({results}: Props) => {

  return (
    <div>
      <div className="row row-cols-1 row-cols-sm-4 g-0">
        {results.map((result) => (
          <div className="col mb-1" key={result.id} >
            <AlbumCard album={result}/>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ResultsGrid;
