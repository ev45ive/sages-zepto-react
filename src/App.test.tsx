import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { CheckAuth } from "./CheckAuth";
jest.mock("./CheckAuth")

test.skip('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
