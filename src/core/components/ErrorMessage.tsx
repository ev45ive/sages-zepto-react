import React from "react";

export function ErrorMessage({ message: error }: { message: unknown; }) {
    return (
        <div className="d-flex align-items-center">
            <p className="alert alert-danger">
                {error instanceof Error ? error.message : String(error)}
            </p>
        </div>
    );
}
