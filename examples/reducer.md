
```js

[1,2,3,4].reduce((acc,x) => {
    console.log(acc, x , acc+x) 
     return acc + x   
},0)
// 0 1 1
// 1 2 3
// 3 3 6
// 6 4 10
10
```

```js
[1,2,3,4].reduce((state,x) => {
     return { ...state, counter: state.counter + x }
},{
     counter: 0,
     todos:[],
})
{counter: 10, todos: Array(0)}
```

```js
inc = (x=1) => state =>  ({ ...state, counter: state.counter + x });
dec = (x=1) => state =>  ({ ...state, counter: state.counter - x });

[inc(3), dec(2), inc()].reduce((state,fn) => {
     return fn(state)
},{
     counter: 0,
     todos:[],
})
{counter: 2, todos: Array(0)}
```

```js
inc = (payload=1) => ({ type:'INC', payload });
dec = (payload=1) => ({ type:'DEC', payload });
addTodo = (payload='byy milk') => ({ type:'ADD_TODO', payload });

[inc(3), dec(2), addTodo('placki!'), inc()].reduce((state,action) => {
     switch(action.type){
         case 'INC': return { ...state, counter: state.counter + action.payload }
         case 'DEC': return { ...state, counter: state.counter + action.payload }
         case 'ADD_TODO': return { ...state, todos: [...state.todos,action.payload ] }
         return state;
     }
},{
     counter: 0,
     todos:[],
})
// {counter: 6, todos: Array(1)}
// counter: 6
// todos: ['placki!']
```

```js

inc = (payload=1) => ({ type:'INC', payload });
dec = (payload=1) => ({ type:'DEC', payload });
addTodo = (payload='byy milk') => ({ type:'ADD_TODO', payload });

reducer = (state, action) => {
     switch(action.type){
         case 'INC': return { ...state, counter: state.counter + action.payload }
         case 'DEC': return { ...state, counter: state.counter + action.payload }
         case 'ADD_TODO': return { ...state, todos: [...state.todos,action.payload ] }
         return state;
     }
}

state = {
     counter: 0,
     todos:[],
}
state = reducer(state, inc(3))
state = reducer(state, dec(2))

setTimeout(()=> { state = reducer(state, addTodo('placki!')) }, 2000)
state = reducer(state, inc())
    
// {counter: 6, todos: Array(0)}
state 
// {counter: 6, todos: Array(1)}
```