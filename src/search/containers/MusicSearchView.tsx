import { SetStateAction, useState } from "react";
import { fetchAlbumsSearchResults } from "../../core/services/APIServices";
import ResultsGrid from "../components/ResultsGrid";
import SearchForm from "../components/SearchForm";
import { useQuery } from "react-query";
import { useNavigate, useSearchParams } from "react-router-dom";

type Props = {};

const MusicSearchView = (props: Props) => {
  // const navigate = useNavigate();

  const [params, updateParams] = useSearchParams();
  const query = params.get("q");

  const { isLoading, error, data } = useQuery(["searchAlbums", query], () =>
    query ? fetchAlbumsSearchResults(query) : undefined
  );

  const search = (query: string | null) => {
    updateParams({ q: query || "" }, { replace: true });
  };

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={search} query={query} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {error instanceof Error && (
            <p className="alert alert-danger">{error.message}</p>
          )}
          {isLoading && <p className="alert alert-info">Loading...</p>}
          {data && <ResultsGrid results={data} />}
        </div>
      </div>
    </div>
  );
};

export default MusicSearchView;
