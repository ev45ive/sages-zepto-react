// https://www.npmjs.com/package/clsx
export const cls = (...classes: (string | boolean)[]) =>
  classes.filter((c) => c !== false).join(" ");
