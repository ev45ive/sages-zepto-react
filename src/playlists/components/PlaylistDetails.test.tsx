import { render, screen, logRoles } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { playlistsData } from "../containers/playlistsData";
import PlaylistDetails from "./PlaylistDetails";

test("adds 1 + 2 to equal 3", () => {
  expect(1 + 2).toBe(3);
});

describe("PlaylistDetails", () => {
  it('show "no playlist selected"', () => {
    render(<PlaylistDetails playlist={undefined} onEdit={() => {}} />);

    screen.getByText("No playlist selected");
  });

  it("shows selected playlist details", () => {
    const editSpy = jest.fn();
    render(<PlaylistDetails playlist={playlistsData[0]} onEdit={editSpy} />);

    screen.getByText("Playlist 123");
    screen.getByTestId("playlist_name");
    const publicEl = screen.getByRole("definition", { name: "Public" });
    expect(publicEl).toHaveTextContent("Yes");

    const btnEl = screen.getByRole("button", { name: "Edit" });
    userEvent.click(btnEl);

    expect(editSpy).toHaveBeenCalled()

    // logRoles(document.body);
  });
});
