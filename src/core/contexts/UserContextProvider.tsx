import React, { useEffect, useMemo, useState } from "react";
import { useAuth } from "../hooks/useAuth";
import { UserProfile } from "../model/User";
import { fetchCurrentUser } from "../services/APIServices";
import { UserContext } from "./UserContext";

type Props = { children: React.ReactNode };

export const UserContextProvider = ({ children }: Props) => {
  const [token, getToken, setToken] = useAuth();
  const [user, setUser] = useState<null | UserProfile>(null);

  useEffect(() => {
    if (!token) return;
    login();
  }, [token]);

  const login = async () => {
    if (!token) getToken();
    const user = await fetchCurrentUser();
    setUser(user);
  };
  const logout = () => {
    setToken(undefined)
    setUser(null);
  };

  const context = useMemo(
    () => ({
      user,
      login,
      logout,
    }),
    [user]
  );

  return (
    <UserContext.Provider value={context}>{children}</UserContext.Provider>
  );
};

export default UserContextProvider;
