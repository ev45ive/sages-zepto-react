import React, { useCallback, useEffect, useMemo, useState } from "react";
import PlaylistDetails from "../components/PlaylistDetails";
import PlaylistEditor from "../components/PlaylistEditor";
import PlaylistList from "../components/PlaylistList";
import { Playlist } from "../../core/model/Playlist";
import { playlistsData } from "./playlistsData";
import { getUUID } from "./getUUID";
import { useSearchParams } from "react-router-dom";
import { useQuery } from "react-query";
import { fetchPlaylists } from "../../core/services/APIServices";

type Props = {};

const PlaylistsView = (props: Props) => {
  const [params] = useSearchParams();
  const id = params.get("id") || undefined;

  const { data: playlists, error } = useQuery(["playlists"], () =>
    fetchPlaylists()
  );
  // const [playlists, setPlaylists] = useState(() => playlistsData);

  const [mode, setMode] = useState<"details" | "editor" | "create">("details");

  const [selectedPlaylist, setSelectedPlaylist] =
    useState<Playlist | undefined>();
  const [selectedId, setSelectedId] = useState<Playlist["id"] | undefined>(id);

  const showDetails = useCallback(() => setMode("details"), []);
  const showChangeEditor = useCallback(() => setMode("editor"), []);
  const showCreateEditor = useCallback(() => {
    setMode("create");
    setSelectedId(undefined);
  }, []);

  const savePlaylist = useCallback(
    (draft: Playlist) => {
      setSelectedId(draft.id);
      // setPlaylists((list) => list.map((p) => (p.id === draft.id ? draft : p)));
      showDetails();
    },
    [showDetails]
  );

  const createPlaylist = useCallback(
    (draft: Playlist) => {
      draft.id = getUUID();
      setSelectedId(draft.id);
      // setPlaylists((playlists) => [...playlists, draft]);
      showDetails();
    },
    [showDetails]
  );

  const deletePlaylist = useCallback(
    (id: Playlist["id"]) => {
      // setPlaylists((playlists) => playlists.filter((p) => p.id !== id));
      setSelectedId((selectedId) =>
        selectedId === id ? undefined : selectedId
      );
      showDetails();
    },
    [showDetails]
  );

  const selectPlaylist = useCallback(
    (id: Playlist["id"]) => setSelectedId(id),
    []
  );

  const EMPTY_PLAYLIST: Playlist = useMemo(
    () => ({
      id: "",
      name: "",
      public: false,
      description: "",
    }),
    []
  );

  useEffect(() => {
    // setSelectedPlaylist(playlists.find((p) => p.id === selectedId));
  }, [selectedId, playlists]);

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList
            selectedId={selectedId}
            playlists={playlists || []}
            onSelected={selectPlaylist}
            onRemove={deletePlaylist}
          />

          <button
            className="btn btn-info float-end mt-3"
            onClick={showCreateEditor}
          >
            Create New
          </button>
        </div>
        <div className="col">
          {mode === "details" && (
            <PlaylistDetails
              playlist={selectedPlaylist}
              onEdit={showChangeEditor}
            />
          )}

          {mode === "editor" && (
            <PlaylistEditor
              playlist={selectedPlaylist}
              onCancel={showDetails}
              onSubmit={savePlaylist}
            />
          )}

          {mode === "create" && (
            <PlaylistEditor
              playlist={EMPTY_PLAYLIST}
              onCancel={showDetails}
              onSubmit={createPlaylist}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default PlaylistsView;
