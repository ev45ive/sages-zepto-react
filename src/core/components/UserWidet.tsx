import React, { useContext } from "react";
import { UserContext } from "../contexts/UserContext";

export function UserWidet() {
  const { user, login, logout } = useContext(UserContext);

  if (user) {
    return (
      <span>
        Welcome {user.display_name}, <span onClick={logout}>Logout</span>
      </span>
    );
  }

  return (
    <span>
      Welcome Guest, <span onClick={login}>Login</span>
    </span>
  );
}
