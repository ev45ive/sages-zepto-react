import React, { useEffect, useRef, useState } from "react";
import { Playlist } from "../../core/model/Playlist";

type Props = {
  playlist?: Playlist;
  onSubmit: (draft: Playlist) => void;
  onCancel: () => void;
};

const EMPTY_PLAYLIST: Playlist = {
  id: "",
  name: "",
  public: false,
  description: "",
};

const PlaylistEditor = React.memo(
  ({ playlist = EMPTY_PLAYLIST, onCancel, onSubmit }: Props) => {
    const [playlistName, setPlaylistName] = useState(playlist.name);
    const [playlistPublic, setPlaylistPublic] = useState(playlist.public);
    const [playlistDescription, setPlaylistDescription] = useState(
      playlist.description
    );
    const nameInputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
      setPlaylistName(playlist.name);
      setPlaylistPublic(playlist.public);
      setPlaylistDescription(playlist.description);
    }, [playlist]);

    useEffect(() => {
      nameInputRef.current?.focus();
    }, [playlist]);

    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setPlaylistName(event.currentTarget.value);
    };

    const submit = (
      e: React.MouseEvent<HTMLButtonElement, MouseEvent>
    ): void => {
      onSubmit({
        ...playlist,
        name: playlistName,
        public: playlistPublic,
        description: playlistDescription,
      });
    };

    return (
      <div>
        <pre>{JSON.stringify(playlist, null, 2)}</pre>
        <div className="form-group mb-3">
          <label htmlFor="playlist_name">Name:</label>
          <input
            type="text"
            className="form-control"
            name="playlist_name"
            id="playlist_name"
            value={playlistName}
            onChange={handleNameChange}
            placeholder="Name"
            ref={nameInputRef}
          />
          <small className="form-text text-muted">
            {playlistName?.length} / 100
          </small>
        </div>

        <div className="form-check mb-3">
          <label className="form-check-label">
            <input
              type="checkbox"
              className="form-check-input"
              name="playlist_public"
              id="playlist_public"
              checked={playlistPublic}
              onChange={(event) =>
                setPlaylistPublic(event.currentTarget.checked)
              }
            />
            Public
          </label>
        </div>

        <div className="form-group mb-3">
          <label htmlFor="playlist_description">Description</label>
          <textarea
            className="form-control"
            name="playlist_description"
            id="playlist_description"
            rows={3}
            value={playlistDescription}
            onChange={(event) =>
              setPlaylistDescription(event.currentTarget.value)
            }
          ></textarea>
        </div>
        <button className="btn btn-danger" onClick={onCancel}>
          Cancel
        </button>
        <button className="btn btn-success" onClick={submit}>
          Save
        </button>
      </div>
    );
  }
);

export default PlaylistEditor;
