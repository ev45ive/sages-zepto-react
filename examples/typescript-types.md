```ts


interface User {
  id: string;
  name: string;
  pet: {
    name: string;
  };
  color: string;
}

const users: User[] = [
  {
    id: "123",
    name: "Alice",
    pet: { name: "Dog" },
    color: "red",
  },
  {
    id: "234",
    name: "Bob",
    pet: { name: "Fish" },
    color: "blue",
  },
  {
    id: "345",
    name: "Kate",
    pet: { name: "Cat" },
    color: "green",
  },
];

```