import React, { useEffect, useState } from "react";

type Props = {
  query: string | null;
  onSearch: (query: string | null) => void;
};

const SearchForm = ({ onSearch, query: parentQuery }: Props) => {
  const [localQuery, setLocalQuery] = useState(parentQuery);

  useEffect(() => setLocalQuery(parentQuery), [parentQuery]);

  useEffect(() => {
    if (parentQuery === localQuery) return;
    
    const handler = setTimeout(() => onSearch(localQuery), 500);
    return () => clearTimeout(handler);
  }, [localQuery, parentQuery]);

  const search = () => {
    onSearch(localQuery);
  };

  return (
    <div className="mb-3">
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          value={localQuery || ""}
          onChange={(e) => setLocalQuery(e.currentTarget.value)}
        />

        <button
          className="btn btn-outline-secondary"
          type="button"
          onClick={search}
        >
          Search
        </button>
      </div>
    </div>
  );
};

export default SearchForm;
