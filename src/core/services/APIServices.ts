import { FullAlbum, PagingObject } from './../model/Search';
import axios from "axios"
import { Album, AlbumSearchResponse } from "../model/Search"
import { UserProfile } from '../model/User';
import { Playlist } from '../model/Playlist';

export const fetchCurrentUser = async () => {
    const resp = await axios.get<UserProfile>("/me", {})
    return resp.data
}


export const fetchAlbums = async () => {
    const resp = await fetch("http://localhost:3000/albums.json")
    return await resp.json()
}

export const fetchAlbumById = async (id: string | null) => {
    if (!id) throw new Error('Missing album Id')
    const resp = await axios.get<unknown>("/albums/" + id, {})
    assertAlbumResponse(resp.data)
    return resp.data
}


export const fetchAlbumsSearchResults = async (query: string) => {
    const resp = await axios.get<unknown>("/search", {
        params: { type: 'album', q: query },
    })
    assertAlbumSearchResponse(resp.data)
    return resp.data.albums.items
}

export const fetchPlaylists = async () => {
    const resp = await axios.get<PagingObject<Playlist>>("/me/playlists", {})
    return resp.data.items
}


/* Validators */
// https://github.com/colinhacks/zod#parse

function assertAlbumSearchResponse(res: any): asserts res is AlbumSearchResponse {
    if (!('albums' in res && 'items' in res.albums && Array.isArray(res.albums.items)))
        throw new Error('Unexpected server response')
}

function assertAlbumResponse(res: any): asserts res is FullAlbum {
    if (!('type' in res && res.type === 'album'))
        throw new Error('Unexpected server response')
}