import React from "react";
import { useQuery } from "react-query";
import { useParams, useSearchParams } from "react-router-dom";
import { fetchAlbumById } from "../../core/services/APIServices";
import AlbumCard from "../components/AlbumCard";
import { ErrorMessage } from "../../core/components/ErrorMessage";
import { Loading } from "../../core/components/Loading";

//  http://localhost:3000/music/albums?id=5Tby0U5VndHW0SomYO7Id7
type Props = {};

const AlbumDetailsView = (props: Props) => {
  const { albumId } = useParams();

  const { data, error, refetch } = useQuery(["albumDetails", albumId], () =>
    albumId ? fetchAlbumById(albumId) : undefined
  );

  if (error) return <ErrorMessage message={error} />;
  if (!data) return <Loading />;

  return (
    <div>
      <div className="row mb-3">
        <div className="col">
          <h1 className="display-3">{data.name}</h1>
          <div className="text-muted">{data.id}</div>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <AlbumCard album={data} />
        </div>
        <div className="col">
          <dl>
            <dt>Name:</dt>
            <dd>{data.name}</dd>

            <dt>Artist:</dt>
            <dd>{data.artists[0]?.name || "Unknown"}</dd>

            <dt>Release date:</dt>
            <dd>{data.release_date}</dd>
          </dl>
        </div>
      </div>
    </div>
  );
};

export default AlbumDetailsView;
