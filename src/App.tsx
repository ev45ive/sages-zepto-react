import "bootstrap/dist/css/bootstrap.css";
import MusicSearchView from "./search/containers/MusicSearchView";
import { Navigate, Route, Routes } from "react-router-dom";
import PlaylistsView from "./playlists/containers/PlaylistsView";
import NavBar from "./core/components/NavBar";
import AlbumDetailsView from "./search/containers/AlbumDetailsView";

function App() {
  return (
    <div>
      <NavBar />
      <div className="container">
        <div className="row">
          <div className="col">
            <Routes>
              <Route path="/" element={<Navigate to="/music/search" />} />
              <Route path="/music/search" element={<MusicSearchView />} />
              <Route
                path="/music/albums/:albumId"
                element={<AlbumDetailsView />}
              />
              <Route path="/playlists" element={<PlaylistsView />} />
              <Route
                path="*"
                element={
                  <h1 className="display-1 mx-auto mt-5 text-center">
                    404 Not Found
                  </h1>
                }
              />
            </Routes>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
