// tsrafce
import React from "react";
import { Playlist } from "../../core/model/Playlist";

type Props = { playlist?: Playlist; onEdit: () => void };

const PlaylistDetails = ({ playlist, onEdit }: Props) => {
  if (!playlist) {
    return <p className="alert alert-info">No playlist selected</p>;
  }

  return (
    <div>
      <dl>
        <dt>Name:</dt>
        <dd data-testid="playlist_name">{playlist.name} </dd>

        <dt>Public:</dt>
        <dd
          aria-label="Public"
          style={{ color: playlist.public ? "red" : "green" }}
        >
          {playlist.public ? "Yes" : "No"}
        </dd>

        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>

      <button className="btn btn-info" onClick={onEdit}>
        Edit
      </button>
    </div>
  );
};

export default PlaylistDetails;
