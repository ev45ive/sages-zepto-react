```tsx



const Section2 = (props: { user: User; children: any }) => (
  <div id="123" title={props.user.name}>
    <p
      className="list-group-item"
      style={{
        color: props.user.color,
        borderBottom: "1px solid",
      }}
    >
      {props.user.name} has {props.user.pet.name}
      {props.children}
    </p>
  </div>
);

const SectionList2 = ({ users }: { users: User[] }) => (
  <ul>
    {/* { users.map((user) => <li key={user.id}> {Section()} </li>} */}
    {users.map((user) => (
      <li key={user.id}>
        <Section2 user={user}> i placki </Section2>
      </li>
    ))}
  </ul>
);

ReactDOM.render(
  // React.createElement(SectionList, { users: users }),
  <SectionList2 users={users} />,
  document.getElementById("root")
);
```