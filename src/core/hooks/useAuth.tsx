import { useLayoutEffect, useMemo } from "react";
import { useOAuth2Token } from "react-oauth2-hook";
import axios from "axios";

export const useAuth = () => {
  useMemo(() => {
    axios.defaults.baseURL = "https://api.spotify.com/v1";
  }, []);

  const [token, getToken, setToken] = useOAuth2Token({
    authorizeUrl: "https://accounts.spotify.com/authorize",
    scope: [""],
    clientID: "f42ec9bb1f84439691a447472efc4302",
    redirectUri: document.location.origin + "/callback",
  });

  useMemo(() => {
    axios.interceptors.response.use(
      (ok) => ok,
      (error) => {
        if (!axios.isAxiosError(error)) {
          throw new Error("Unexpected server error");
        }
        if (error.response && isSpotifyError(error.response.data)) {
          throw new Error(error.response?.data.error.message);
        }
        if (error.response?.status === 401) {
          getToken();
        }
        throw new Error("Unexpected error");
      }
    );
  }, []);

  const handlerId = useMemo(() => {
    if (!token) return 0;

    return axios.interceptors.request.use((config) => {
      config.headers = {
        Authorization: "Bearer " + token,
        ...config.headers,
      };
      return config;
    });
  }, [token]);

  useLayoutEffect(
    () => axios.interceptors.request.eject(handlerId),
    [handlerId]
  );

  return [token, getToken, setToken] as const;
};

interface SpotifyError {
  error: { message: string; status: number };
}
function isSpotifyError(res: any): res is SpotifyError {
  return (
    "error" in res &&
    "status" in res.error &&
    "message" in res.error &&
    typeof res.error.message === "string"
  );
}
