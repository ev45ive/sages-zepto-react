export function getUUID(): string {
  return crypto.randomUUID();
}
