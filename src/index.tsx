import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { ErrorBoundary } from "./core/components/ErrorBoundary";
import { QueryClient, QueryClientProvider } from "react-query";
// import { HashRouter as Router } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import UserContextProvider from "./core/contexts/UserContextProvider";
import { CheckAuth } from "./CheckAuth";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 2,
    },
  },
});

ReactDOM.render(
  (() => {
    if (window.location.hash.includes("access_token")) return <CheckAuth />;
    return (
      <React.StrictMode>
        <ErrorBoundary>
          <QueryClientProvider client={queryClient}>
            <UserContextProvider>
              <Router>
                <App />
              </Router>
            </UserContextProvider>
          </QueryClientProvider>
        </ErrorBoundary>
      </React.StrictMode>
    );
  })(),
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
