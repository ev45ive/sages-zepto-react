import React from "react";
import { Playlist } from "../../core/model/Playlist";
import { cls } from "./cls";

type Props = {
  selectedId?: string;
  playlists: Playlist[];
  onSelected: (id: string) => void;
  onRemove: (id: string) => void;
};

const PlaylistList = React.memo(
  ({ playlists, selectedId, onSelected, onRemove }: Props) => {
    const selectById = (id: string) => {
      onSelected(id);
    };

    return (
      <div>
        <div className="list-group">
          {playlists.map((playlist, index) => (
            <button
              className={cls(
                "list-group-item",
                "list-group-item-action",
                playlist.id === selectedId && "active"
              )}
              key={playlist.id}
              onClick={(event /* SyntheticBaseEvent */) => {
                // debugger
                selectById(playlist.id);
              }}
            >
              {index + 1}. {playlist.name}
              <span
                className="float-end close"
                onClick={(event) => {
                  // debugger
                  event.stopPropagation();
                  onRemove(playlist.id);
                }}
              >
                &times;
              </span>
            </button>
          ))}
        </div>
      </div>
    );
  },
  /* propsAreEqual?:  */
  // (prevProps: Readonly<Props>, nextProps: Readonly<Props>) => true
  // (prevProps: Readonly<Props>, nextProps: Readonly<Props>) =>
  //   prevProps.playlists === nextProps.playlists &&
  //   prevProps.selectedId === nextProps.selectedId
);

export default PlaylistList;
