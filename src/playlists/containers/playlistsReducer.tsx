import { Playlist } from "../../core/model/Playlist";
import { playlistsData } from "./playlistsData";

type State = {
  mode: "details" | "editor" | "create";
  playlists: Playlist[];
  selectedId?: Playlist["id"];
  counter: number;
};

export const initialState: State = {
  mode: "details",
  playlists: playlistsData,
  counter: 0,
};

export const counterReducer = (state = 0, action: Action) => {
  switch (action.type) {
    case "[Counter] Increment":
      return state + action.payload.value;
    case "[Counter] Decrement":
      return state - action.payload.value;
    default:
      return state;
  }
};

export const playlistsReducer = (
  state: Readonly<State>,
  action: Action
): State => {
  console.log(action, state);
  switch (action.type) {
    case "[Playlist] SELECT":
      return { ...state, selectedId: action.payload.id };
    case "[Playlist] DELETE":
      return {
        ...state,
        playlists: state.playlists.filter((p) => p.id !== action.payload.id),
      };
    default:
      return {
        ...state,
        // Reducer state 'slice'
        counter: counterReducer(state.counter, action),
      };
  }
};

// https://github.com/redux-utilities/redux-actions
export const playlistsSelect = (id: string) =>
  ({
    type: "[Playlist] SELECT",
    payload: { id },
  } as const);

export const playlistsDelete = (id: string) =>
  ({
    type: "[Playlist] DELETE",
    payload: { id },
  } as const);

export const inc = (value = 1) =>
  ({
    type: "[Counter] Increment",
    payload: { value },
  } as const);

export const dec = (value = 1) =>
  ({
    type: "[Counter] Decrement",
    payload: { value },
  } as const);

type Action = ReturnType<
  typeof playlistsSelect | typeof playlistsDelete | typeof inc | typeof dec
>;
