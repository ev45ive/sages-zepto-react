import { useEffect, useState } from "react";


export function useFetch<T, P extends Array<any>>(
  params: P,
  fetcherFn: (...params: P) => Promise<T>
) {
  const [data, setData] = useState<T | undefined>();
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setData(undefined);
    setError("");
    setIsLoading(true);
    fetcherFn(...params)
      .then((resp) => setData(resp))
      .catch((error) => setError(error.message))
      .finally(() => setIsLoading(false));
  }, params);

  return { error, isLoading, data };
}
