import { Playlist } from "../../core/model/Playlist";

export const playlistsData: Playlist[] = [
    {
        id: "123",
        name: "Playlist 123",
        public: true,
        description: "awesome playlist",
    },
    {
        id: "234",
        name: "Playlist 234",
        public: false,
        description: "the Best playlist",
    },
    {
        id: "345",
        name: "Playlist 345",
        public: true,
        description: "my playlist",
    },
];
