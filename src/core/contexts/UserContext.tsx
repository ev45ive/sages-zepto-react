import React from "react";
import { UserProfile } from "../model/User";

interface UserContext {
  user: null | UserProfile;
  login(): void;
  logout(): void;
}

export const UserContext = React.createContext<UserContext>({
  user: null,
  login() {
    throw "No Context provider ";
  },
  logout() {
    throw "No Context provider ";
  },
});
