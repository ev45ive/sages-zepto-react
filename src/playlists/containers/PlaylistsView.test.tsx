import { render, screen } from "@testing-library/react";
import { QueryClient, QueryClientProvider } from "react-query";
import { MemoryRouter as Router } from "react-router-dom";
import { fetchPlaylists } from "../../core/services/APIServices";
import { getUUID } from "./getUUID";
import { playlistsData } from "./playlistsData";
import PlaylistsView from "./PlaylistsView";

jest.mock("../../core/services/APIServices");
jest.mock("./getUUID");

describe("PlaylistsView", () => {
  const setup = () => {
    const client = new QueryClient();
    (fetchPlaylists as jest.Mock).mockResolvedValue(playlistsData);
    render(
      <Router
        basename="/playlists"
        initialEntries={["/search", "/playlists?id=234"]}
      >
        <QueryClientProvider client={client}>
          <PlaylistsView />
        </QueryClientProvider>
      </Router>
    );
  };

  it("loads data from server", async () => {
    setup();

    const item = await screen.findAllByRole("button", {
      name: /Playlist/,
    });
    expect(fetchPlaylists).toHaveBeenCalled();
    expect(item).toHaveLength(playlistsData.length);
  });

  it("shows playlists", async () => {
    
    setup();
    // const items = screen.queryAllByRole("button", {
    //   name: /Playlist/,
    // });
    // items.find(btn => btn.className.includes('active'))

    const item = await screen.findByRole("button", {
      name: /Playlist 234/,
    });
    expect(item).toHaveClass("active");
    // console.log(item);

    screen.debug();
  });
});
