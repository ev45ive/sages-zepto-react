```tsx
// RFC
const Section = (props: { user: User; children: any }) =>
  React.createElement(
    "div",
    { id: 123, title: props.user.name },
    React.createElement(
      "p",
      {
        className: "list-group-item",
        style: { color: props.user.color, borderBottom: "1px solid" },
      },
      `${props.user.name} has ${props.user.pet.name}`,
      props.children
    )
  );

const SectionList = ({ users }: { users: User[] }) =>
  React.createElement(
    "ul",
    {},
    users.map((user) =>
      // React.createElement("li", { key: user.id }, Section({user:user}))
      React.createElement(
        "li",
        { key: user.id },
        React.createElement(Section, { user: user } as any, " & placki")
      )
    )
  );

ReactDOM.render(
  React.createElement(SectionList, { users: users }),
  document.getElementById("root")
);
```
