import { useCallback, useMemo, useReducer } from "react";
import PlaylistDetails from "../components/PlaylistDetails";
import PlaylistList from "../components/PlaylistList";
import {
  playlistsReducer,
  initialState,
  playlistsSelect,
  playlistsDelete,
} from "./playlistsReducer";

type Props = {};

const PlaylistsReducerView = (props: Props) => {
  const [state, dispatch] = useReducer(playlistsReducer, initialState);

  const { mode, playlists, selectedId } = state;
  const selectedPlaylist = useMemo(
    () => playlists.find((p) => p.id === selectedId),
    [playlists, selectedId]
  );

  const selectPlaylist = useCallback(
    (id: string) => dispatch(playlistsSelect(id)),
    []
  );
  const deletePlaylist = useCallback(
    (id: string) => dispatch(playlistsDelete(id)),
    []
  );
  const showChangeEditor = useCallback(() => {}, []);

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList
            selectedId={selectedId}
            playlists={playlists}
            onSelected={selectPlaylist}
            onRemove={deletePlaylist}
          />

          {/* <button
            className="btn btn-info float-end mt-3"
            onClick={showCreateEditor}
          >
            Create New
          </button> */}
        </div>
        <div className="col">
          {mode === "details" && (
            <PlaylistDetails
              playlist={selectedPlaylist}
              onEdit={showChangeEditor}
            />
          )}

          {/* {mode === "editor" && (
            <PlaylistEditor
              playlist={selectedPlaylist}
              onCancel={showDetails}
              onSubmit={savePlaylist}
            />
          )} */}

          {/* {mode === "create" && (
            <PlaylistEditor
              playlist={EMPTY_PLAYLIST}
              onCancel={showDetails}
              onSubmit={createPlaylist}
            />
          )} */}
        </div>
      </div>
    </div>
  );
};

export default PlaylistsReducerView;
