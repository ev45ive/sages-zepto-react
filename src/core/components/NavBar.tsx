import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { cls } from "../../playlists/components/cls";
import { UserWidet } from "./UserWidet";

type Props = {};

const NavBar = (props: Props) => {
  const [open, setOpen] = useState(false);
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
      <div className="container">
        <NavLink
          className={({ isActive }) =>
            "navbar-brand " + (isActive ? "active " : "")
          }
          to="/"
        >
          MusicApp
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          onClick={() => setOpen(!open)}
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className={cls("collapse navbar-collapse", open && "show")}>
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink className="nav-link" to="/music/search">
                Search
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/playlists">
                Playlists
              </NavLink>
            </li>
          </ul>
          <div className="ms-auto navbar-text">
            <UserWidet />
          </div>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;


